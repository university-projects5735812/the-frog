package nl.saxion.ptbc.Map2D;

import nl.saxion.ptbc.ObjectDisplay.Point2D;
import static nl.saxion.ptbc.shared.Shared.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.*;

/**
 * Represents a map that displays points on a 2D image.
 */
public class Map extends JPanel {
    private BufferedImage backgroundImage;
    private ArrayList<Point2D> points;

    /**
     * Constructs a Map object with the specified list of points.
     *
     * @param points The list of points to be displayed on the map.
     */
    public Map(ArrayList<Point2D> points) {
        this.points = points;

        try {
            backgroundImage = ImageIO.read(new File("shared/resources/MoonMap.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * Displays the points on the map and saves the output image.
     */
    public void displayPointsOnMap() {
        if (backgroundImage == null) {
            System.out.println("Background image not found.");
            return;
        }

        double mapWidth = 421.29 + 78.75;
        double mapHeight = 354.13 + 145.892;

        int outputWidth = backgroundImage.getWidth();
        int outputHeight = backgroundImage.getHeight();

        BufferedImage scaledImage = new BufferedImage(outputWidth, outputHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = scaledImage.createGraphics();

        g2d.setColor(Color.WHITE);
        g2d.fillRect(0, 0, outputWidth, outputHeight);
        g2d.drawImage(backgroundImage, 0, 0, null);

        g2d.setColor(Color.RED);
        for (Point2D point : points) {
        double x = point.getX();
        double z = point.getZ();

        double scaledX = (z - MAP_2D_BOTTOM_LEFT_Z) * outputHeight/ mapHeight;
        double scaledZ =  (x - MAP_2D_BOTTOM_LEFT_X) * outputWidth / mapWidth;

        if (scaledX >= 0 && scaledX < outputWidth && scaledZ >= 0 && scaledZ < outputHeight) {
            g2d.fillOval((int) (scaledX - 2), (int) (scaledZ - 2), 4, 4);
           }
        }

        g2d.dispose();

        try {
            ImageIO.write(scaledImage, "jpg", new File("output_image.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        if (backgroundImage == null) {
            System.out.println("Background image not found.");
            return;
        }

        double mapWidth = 421.25 + 78.75;
        double mapHeight = 354.13 + 145.892;

        int outputWidth = 500;
        int outputHeight = 500;

        Graphics2D g2d = (Graphics2D) g;

        g2d.setColor(Color.WHITE);
        g2d.fillRect(0, 0, outputWidth, outputHeight);
        g2d.drawImage(backgroundImage, 0, 0, 500,500,null);

        g2d.setColor(Color.RED);
        for (Point2D point : points) {
            double x = point.getX();
            double z = point.getZ();

            double scaledX = (z - MAP_2D_BOTTOM_LEFT_Z) * outputHeight / mapHeight;
            double scaledZ = (x - MAP_2D_BOTTOM_LEFT_X) * outputWidth / mapWidth;

            if (scaledX >= 0 && scaledX < outputWidth && scaledZ >= 0 && scaledZ < outputHeight) {
                g2d.fillOval((int) (scaledX - 1), (int) (scaledZ - 1), 2, 2);
            }
        }
    }

    /**
     * Main method for testing the map display.
     */
    public void main() {

        JFrame frame = new JFrame("Map Display");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setSize(500, 500);

        Map map = new Map(points);
        frame.add(map);

        frame.setVisible(true);
    }
}

