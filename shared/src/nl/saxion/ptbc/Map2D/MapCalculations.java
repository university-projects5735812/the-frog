package nl.saxion.ptbc.Map2D;

import nl.saxion.ptbc.ObjectDisplay.Point2D;
import nl.saxion.ptbc.ObjectDisplay.Point3D;

import java.util.ArrayList;

/**
 * Contains methods for map calculations.
 */
public class MapCalculations {
    /**
     * Converts relative coordinates of objects to absolute coordinates based on the radar position.
     *
     * @param objects List of objects with relative coordinates.
     * @param radar   Radar position with x, y, and z coordinates.
     * @return List of objects with absolute coordinates.
     */
    public static ArrayList<Point2D> convertToAbsoluteCoordinates(ArrayList<Point3D> objects, Point3D radar) {
        ArrayList<Point2D> absolutePoints = new ArrayList<>();
        double radian = Math.toRadians(radar.getY());
        for (Point3D object : objects) {
            double x = radar.getX() + object.getX() * Math.cos(radian) + object.getZ() * Math.sin(radian);
            double z = radar.getZ() + object.getZ() * Math.cos(radian) - object.getX() * Math.sin(radian);
            absolutePoints.add(new Point2D(x, z));
        }
        return absolutePoints;
    }

    /**
     * Main method for testing the conversion of coordinates.
     *
     * @param args Command-line arguments.
     */
    public static void main(String[] args) {
        ArrayList<Point3D> point3DS = new ArrayList<>();
        Point3D point3D = new Point3D(5, 5, 0);
        point3DS.add(point3D);
        ArrayList<Point2D> point2DS = convertToAbsoluteCoordinates(point3DS, new Point3D(200, 200, 62));
        System.out.println(point2DS.get(0).getX() + " " + point2DS.get(0).getZ());
    }
}
