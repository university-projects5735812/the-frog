package nl.saxion.ptbc.shared;

import java.util.Scanner;

public class Shared {
    public static final String COMMUNICATION_SERVER = "localhost";
    public static final int COMMUNICATION_PORT = 50000;

    //Pilot
    public static final int PILOT_SCREEN_WIDTH = 700;
    public static final int PILOT_SCREEN_HEIGHT = 500;

    //Ground Control
    public static final int TILE_SIZE = 10; //10x10 width*height, this can serve as a scaler
    public static final int TILES_IN_WIDTH = 98;
    public static final int TILES_IN_HEIGHT = 33;
    public static final int GROUND_CONTROL_SCREEN_WIDTH = TILE_SIZE * TILES_IN_WIDTH; // Screen width
    public static final int GROUND_CONTROL_SCREEN_HEIGHT = TILE_SIZE * TILES_IN_HEIGHT; // Screen height
    public static final int MAP_2D_ACTUAL_WIDTH = 1115; //Map
    public static final int MAP_2D_ACTUAL_HEIGHT = 1115; //Map
    public static final int MAP_2D_UI_WIDTH_GROUND_CONTROL = MAP_2D_ACTUAL_WIDTH / 5 + 87; //Map: 223 + 87 = 310
    public static final int MAP_2D_UI_HEIGHT_GROUND_CONTROL = MAP_2D_ACTUAL_HEIGHT / 5 + 87; //Map: 223 + 87 = 310
    public static final int MAP_2D_UI_WIDTH_PILOT = MAP_2D_ACTUAL_WIDTH / 5 + 22; //Map: 223 + 42 = 265

    public static final int MAP_2D_UI_HEIGHT_PILOT = MAP_2D_ACTUAL_HEIGHT / 5 + 22; //Map: 223 + 42 = 265

    public static final double MAP_2D_BOTTOM_LEFT_X = -78.75;
    public static final double MAP_2D_TOP_RIGHT_X = 421.29;
    public static final double MAP_2D_BOTTOM_LEFT_Z = -145.89;
    public static final double MAP_2D_TOP_RIGHT_Z = 354.13;
    public static final double FROG_STARTING_POSITION_X = 257.9739;
    public static final double FROG_STARTING_POSITION_Z = 248.198;
    public static final double STARTING_ANGLE = 62.44;

    public static String readString() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

    public static int readInt() {
        return Integer.parseInt(readString());
    }
}
