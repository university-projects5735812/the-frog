package nl.saxion.ptbc.dbUtil;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * The dbConnection class provides methods for establishing a connection to the database.
 */
public class dbConnection {
    private static final String SQCONN = "jdbc:sqlite:ground_control/resources/Database.sqlite";

    /**
     * Retrieves a connection to the database.
     *
     * @return a Connection object representing the database connection
     * @throws SQLException if a database access error occurs
     */
    public static Connection getConnection() throws SQLException {
        try {
            Class.forName("org.sqlite.JDBC");
            return DriverManager.getConnection(SQCONN);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
