package nl.saxion.ptbc.ground_control.automaticMovement;

import nl.saxion.ptbc.ground_control.GroundControlPanel;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Point2D;

import static nl.saxion.ptbc.shared.Shared.*;

public class MapListener implements MouseListener {

    private GroundControlPanel groundControlPanel;

    public MapListener(GroundControlPanel pilotDashboard) {
        this.groundControlPanel = pilotDashboard;
    }

    /**
     * When the mouse is clicked, convert the position of the mouse on UI to the position of the Frog on 3D map and order it to move there.
     * @param e the event to be processed
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        if (groundControlPanel.getClickZone().contains(e.getX(), e.getY())) {
            double clickedPointX = (e.getY() - groundControlPanel.getClickZone().y) * (MAP_2D_TOP_RIGHT_X - MAP_2D_BOTTOM_LEFT_X) / MAP_2D_UI_HEIGHT_GROUND_CONTROL + MAP_2D_BOTTOM_LEFT_X;
            double clickedPointY = (e.getX() - groundControlPanel.getClickZone().x) * (MAP_2D_TOP_RIGHT_Z - MAP_2D_BOTTOM_LEFT_Z) / MAP_2D_UI_WIDTH_GROUND_CONTROL + MAP_2D_BOTTOM_LEFT_Z;
            System.out.println("eX: " + e.getX() + " | eY: " + e.getY() + " | clickX: " + clickedPointX + " | clickY: " + clickedPointY);
            Point2D.Double clickedPoint = new Point2D.Double(clickedPointX, clickedPointY);

            groundControlPanel.destination = new Point2D.Double(e.getX(), e.getY());
            groundControlPanel.repaint();

            groundControlPanel.moveAutomaticallyToPoint(clickedPoint);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
