package nl.saxion.ptbc.ground_control;

import nl.saxion.ptbc.ObjectDisplay.Point2D;
import nl.saxion.ptbc.ground_control.automaticMovement.AutomaticMovement;
import nl.saxion.ptbc.ground_control.storageHandler.DatabaseHandler;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class GroundControlDashboard extends JFrame {

    private GroundControl groundControl;
    private GroundControlPanel groundControlPanel;
    private ArrayList<Point2D> allRadarPoints = new ArrayList<>();


    /**
     * Link the dashboard instance with the ground control
     * @param groundControl the ground control can put key word 'this' as param when initialize the dashboard
     */
    public GroundControlDashboard(GroundControl groundControl) {
        this.groundControl = groundControl;
        groundControlPanel = new GroundControlPanel(this);
    }

    /**
     * Set up the window of the UI which is the dashboard
     */
    public void start() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setContentPane(groundControlPanel);
        setResizable(false);
        pack(); //The size of frame is set to same with panel size
        setTitle("Ground Control Dashboard");
        setVisible(true);
        groundControlPanel.requestFocus();
    }

    public void sendMessageToPilot(String message) {
        groundControl.sendMessageToPilot(message, this.groundControl); // Pass the message to the GroundControl instance
    }

    /**
     * Updates the information based on the received message. If the message contains the word "DODGE", it will set
     * automatic movement to dodging the obstacles. If the message contains information related to the Frog Path,
     * it updates the 2D map in the ground control panel. Otherwise, it prints the received message. If the message starts
     * with "RADAR POINT", it extracts the object location and adds it to the list of all radar points. If the message starts
     * with "SENT ALL RADAR POINTS", it sets the list of all radar points in the ground control panel, repaints the panel,
     * and saves the collision points to the database associated with the mission ID.
     * @param message the received message to process and update the information
     */
    public void updateInformation(String message) {
        if (message.contains("DODGE"))
            AutomaticMovement.stopAutomatic = true;

        if (message.contains("Frog Path:") || message.contains("Frog Path First Point: ") || message.contains("Frog Path Last Point: ") || message.contains("Next Angle: ")) {
            groundControlPanel.update2DMap(message);
        } else if (!message.isBlank()) {
            System.out.printf("[Received] %s%n", message);
        }

        if (message.startsWith("RADAR POINT")) {
            Point2D object = takeObjectLocation(message);
            allRadarPoints.add(object);
        }
        if (message.startsWith("SENT ALL RADAR POINTS")) {
            groundControlPanel.setAllRadarPoints(allRadarPoints);
            groundControlPanel.repaint();
            // Call saveCollisionPointsToDatabase with the mission ID
            int missionId = DatabaseHandler.getMissionIdFromDatabase();
            DatabaseHandler.saveCollisionPointsToDatabase(allRadarPoints, missionId);
        }

    }

    /**
     * Adds the new radar points to the list of all radar points.
     *
     * @param newRadarPoints the new radar points to be added
     */
    public void addToAllRadarPoints(ArrayList<Point2D> newRadarPoints) {
        this.allRadarPoints.addAll(newRadarPoints);
    }

    /**
     * Extracts the object location from the radar point message.
     *
     * @param message the radar point message
     * @return the object location as a Point2D object
     */
    public static Point2D takeObjectLocation(String message) {
        String[] values = message.split("RADAR POINT ")[1].split(" ");
        List<Double> convertedValues = new ArrayList<>();
        for (String value : values) {
            convertedValues.add(Double.valueOf(value));
        }
        return new Point2D(convertedValues.get(0), convertedValues.get(1));
    }
}
