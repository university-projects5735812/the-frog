package nl.saxion.ptbc.ground_control.storageHandler;
/**
 * The listener interface for receiving database events.
 */
public interface DatabaseHandlerListener {

    /**
     * Called when a mission is saved to the database.
     *
     * @param message The message indicating the status of the mission save operation.
     */
    void onMissionSaved(String message);
}
