package nl.saxion.ptbc.frog_pilot.PilotGUI;

import nl.saxion.ptbc.ObjectDisplay.MapDisplay;

import static nl.saxion.ptbc.shared.Shared.*;

import nl.saxion.ptbc.frog_pilot.ThePilot;
import nl.saxion.ptbc.frog_pilot.ThePilotPanel;
import nl.saxion.ptbc.shared.Direction;
import nl.saxion.ptbc.frog_pilot.database.DatabasePilotHandler;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PilotDashboard extends JFrame {

    private final PilotDashboardButtonFunctions functions;
    private MapDisplay mapDisplay;
    private final ThePilot pilot;
    private final ThePilotPanel thePilotPanel;
    private JLabel manualBtnLbl;
    protected JButton manualOnOffBtn, reversePowerBtn, reverseAngleBtn, manualCommandSetBtn, exportImportMapBtn, radarOnOffBtn, showCollisionMapBtn, ignoreCollisionBtn;
    protected JTextField powerTxtFld, angleTxtFld, timeTxtFld;
    private boolean isPowerReversed, isAngleReversed;
    private boolean isRadarOn, hasStopped = true;
    private boolean switchManual = false;
    private int collisionDetectionSetting = 1;
    private BufferedImage mapImage;
    private double frogX = FROG_STARTING_POSITION_X;
    private double frogZ = FROG_STARTING_POSITION_Z;
    private Direction direction;
    private double previousAngle = STARTING_ANGLE;
    private List<Point2D.Double> frogPath;
    private List<Double> frogNextAngles;
    private ArrayList<nl.saxion.ptbc.ObjectDisplay.Point2D> allRadarPoints = new ArrayList<>();
    public Point2D.Double destination = new Point2D.Double(-50, -50);


    public PilotDashboard(ThePilot pilot) {
        this.pilot = pilot;
        this.thePilotPanel = new ThePilotPanel(this);
        this.functions = new PilotDashboardButtonFunctions(pilot, this);
        setPilotDashboardInPilot();
    }

    /**
     * Creates and initiates the panel, adds the components.
     */
    public void start() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setContentPane(thePilotPanel);
        addComponents(); // Adding all components

        setResizable(false);
        setLayout(null);
        pack(); //The size of frame is set to same with panel size
        setTitle("Pilot Dashboard");
        setVisible(true);
        manualBtnLbl.setFocusable(true);
        addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                thePilotPanel.focusGained();
            }

            @Override
            public void focusLost(FocusEvent e) {
                thePilotPanel.focusLost();
            }
        });
    }

    /**
     *
     * Create a button in a more compact way.
     * @param X the X of the top left coordinate on the panel.
     * @param Y the Z of the top left coordinate on the panel.
     * @param width  the width of the component.
     * @param height the height of the component.
     * @param text the display text on the label.
     * @return the created component
     */
    private JLabel createLabel(int X, int Y, int width, int height, String text) {
        JLabel label = new JLabel();
        label.setBounds(X, Y, width, height);
        label.setText(text);
        return label;
    }

    /**
     * Create a button in a more compact way.
     * @param X the X of the top left coordinate on the panel.
     * @param Y the Z of the top left coordinate on the panel.
     * @param width  the width of the component
     * @param height the height of the component
     * @param text the display text on the component
     * @param e an event that can be triggered when there's an action. (event) -> {(method)}, to add the method, or just {} if no event is wanted.
     * @return the created component
     */
    private JButton createButton(int X, int Y, int width, int height, String text, ActionListener e) {
        JButton button = new JButton();
        button.setBounds(X, Y, width, height);
        button.setText(text);
        button.addActionListener(e);
        return button;
    }

    /**
     * Create a text field in a more compact way.
     * @param X the X of the top left coordinate on the panel.
     * @param Y the Z of the top left coordinate on the panel.
     * @param width  the width of the component
     * @param height the height of the component
     * @param e an event that can be triggered when there's an action. (event) -> {(method)}, to add the method, or just {} if no event is wanted.
     * @return the created component
     */
    private JTextField createTextField(int X, int Y, int width, int height, ActionListener e) {
        JTextField textField = new JTextField();
        textField.setBounds(X, Y, width, height);
        textField.addActionListener(e);
        return textField;
    }

    /**
     * Creates all labels using createLabel().
     * @return Array with all created labels
     */
    private JLabel[] createLabels() {
        manualBtnLbl = createLabel(9, 5, 100, 20, "Manual Drive:");
        JLabel powerLbl = createLabel(95, 5, 100, 20, "Power: (1/2)");
        JLabel angleLbl = createLabel(205, 5, 100, 20, "Angle: (0/20)");
        JLabel timeLbl = createLabel(315, 5, 100, 20, "Time: (1/2)");
        return new JLabel[]{manualBtnLbl, powerLbl, angleLbl, timeLbl};
    }

    /**
     * Creates all buttons using createButton().
     * @return Array with all created buttons
     */
    private JButton[] createButtons() {
        manualOnOffBtn = createButton(10, 30, 75, PILOT_SCREEN_HEIGHT - 40, "Off", (event) -> functions.setSwitchManual(manualOnOffBtn, thePilotPanel));
        manualCommandSetBtn = createButton(95, 110, 320, 40, "Drive", (event) -> functions.manualCommandSetButton(powerTxtFld, angleTxtFld, timeTxtFld));
        reversePowerBtn = createButton(95, 80, 100, 20, "Forward", (event) -> functions.reversePowerButton(reversePowerBtn));
        reverseAngleBtn = createButton(205, 80, 100, 20, "Right", (event) -> functions.reverseAngleButton(reverseAngleBtn));
        exportImportMapBtn = createButton(425, PILOT_SCREEN_WIDTH - 315, (PILOT_SCREEN_WIDTH - 415) / 2 - 15, 40, "Save collision-point map", (event) -> {
                int missionId = DatabasePilotHandler.getMissionIdFromDatabase();
                DatabasePilotHandler.saveCollisionPointsToDatabase(allRadarPoints, missionId);
        });
        radarOnOffBtn = createButton(425 + (PILOT_SCREEN_WIDTH - 415) / 2 - 5, PILOT_SCREEN_WIDTH - 315, (PILOT_SCREEN_WIDTH - 415) / 2 - 15, 40, "Radar On", (event) -> functions.setIsRadarOn(radarOnOffBtn));
        showCollisionMapBtn = createButton(425, PILOT_SCREEN_WIDTH - 265, PILOT_SCREEN_WIDTH - 435, 55, "Collision Map", (event) -> this.mapDisplay.main());
        ignoreCollisionBtn = createButton(315, 80, 100, 20, "Stop", (event) -> functions.ignoreCollisionButton(ignoreCollisionBtn));

        return new JButton[]{manualOnOffBtn, manualCommandSetBtn, reversePowerBtn, reverseAngleBtn
                , exportImportMapBtn, radarOnOffBtn, showCollisionMapBtn, ignoreCollisionBtn};
    }

    /**
     * Creates all text fields used, using createTextField().
     * @return Array with all created text fields
     */
    private JTextField[] createTextFields() {
        powerTxtFld = createTextField(95, 30, 100, 40, (event) -> functions.addListenerToTextFields(powerTxtFld, new String[]{"1", "2"}));
        angleTxtFld = createTextField(205, 30, 100, 40, (event) -> functions.addListenerToTextFields(angleTxtFld, new String[]{"0", "5", "10", "20"}));
        timeTxtFld = createTextField(315, 30, 100, 40, (event) -> functions.addListenerToTextFields(timeTxtFld, new String[]{"1", "2"}));
        return new JTextField[]{powerTxtFld, angleTxtFld, timeTxtFld};
    }

    /**
     * Adds all components created by the other creation methods.
     */
    private void addComponents() {
        Arrays.stream(createLabels()).forEach(this::add);
        Arrays.stream(createButtons()).forEach(this::add);
        Arrays.stream(createTextFields()).forEach(this::add);

        map2D();
    }

    private void map2D() {
        loadMapImage();
        direction = new Direction("DirectionEdited2.png"); // set up direction

        frogPath = new ArrayList<>();
        frogPath.add(new Point2D.Double(FROG_STARTING_POSITION_X, FROG_STARTING_POSITION_Z));

        frogNextAngles = new ArrayList<>();
        frogNextAngles.add(STARTING_ANGLE);
    }

    /**
     * Load the image of the moonMap to be displayed as map.
     */
    private void loadMapImage() {
        try {
            mapImage = ImageIO.read(new File("shared/resources/MoonMap.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Updates the image of the frog on the 2D map
     * @param x the x of the frog
     * @param z the z of the frog
     * @param angle the rotation angle of the frog
     */
    public void updateFrogOn2DMap(double x, double z, double angle) {
        //Location
        frogX = x;
        frogZ = z;
        Point2D.Double currentPosition = new Point2D.Double(x, z);
        frogPath.add(currentPosition);

        //Direction
        double nextAngle = angle - previousAngle;
        direction.rotateImg(nextAngle);
        previousAngle = angle;
        frogNextAngles.add(angle);

        //Send data to ground control
        send2DMapDataToGroundControl();

        //Re-draw
        repaint();
    }


    /**
     * Sends the map data to the ground control
     */
    private void send2DMapDataToGroundControl() {
        String dataFor2DMap = "";

        for (int i = 0; i < frogPath.size(); i++) {
            if (i == 0) {
                dataFor2DMap += "Frog Path First Point: " + frogPath.get(i).getX() + " " + frogPath.get(i).getY() + "\n";
            } else if (i == frogPath.size() - 1) {
                dataFor2DMap += "Frog Path Last Point: " + frogPath.get(i).getX() + " " + frogPath.get(i).getY() + "\n";
            } else {
                dataFor2DMap += "Frog Path: " + frogPath.get(i).getX() + " " + frogPath.get(i).getY() + "\n";
            }
        }

        dataFor2DMap += "Next Angle: " + frogNextAngles.get(frogNextAngles.size() - 1) + "\n";

        pilot.sendMessageToGroundControl(dataFor2DMap);
    }

    /**
     * Overrides the paint method to customize the rendering of the pilot dashboard.
     * @param g the Graphics object used for painting
     */
    @Override
    public void paint(Graphics g) {
        super.paint(g);

        double mapX = 433;
        double mapY = 58;

        //Draw map
        g.drawImage(mapImage, (int) mapX, (int) mapY, MAP_2D_UI_WIDTH_PILOT, MAP_2D_UI_HEIGHT_PILOT, null);

        // Draw Frog's position
        Point2D.Double frogPos = functions.calculatePosition(frogX, frogZ);

        //Driven route
        g.setColor(Color.YELLOW);
        int pointSize = 3;
        for (Point2D.Double point : frogPath) {
            Point2D.Double previousPoint = functions.calculatePosition(point.getX(), point.getY());
            double pointX = mapX + previousPoint.getX() - pointSize / 2f;
            double pointY = mapY + previousPoint.getY() - pointSize / 2f;
            g.fillOval((int) pointX, (int) pointY, pointSize, pointSize);
        }

        //Set up position
        double width = 16;
        double height = 16;
        double x = mapX + frogPos.getX() - width / 2; // -27
        double y = mapY + frogPos.getY() - height / 2; // -66

        //Set up direction
        direction.draw(g, (int) x, (int) y, (int) width, (int) height);

        //Radar Points
        g.setColor(Color.BLUE);

        for (nl.saxion.ptbc.ObjectDisplay.Point2D point : allRadarPoints) {
            double x_radar = point.getX();
            double z_radar = point.getZ();

            double scaledX = (z_radar - MAP_2D_BOTTOM_LEFT_Z) * MAP_2D_UI_HEIGHT_PILOT / (MAP_2D_TOP_RIGHT_Z - MAP_2D_BOTTOM_LEFT_Z);

            double scaledZ = (x_radar - MAP_2D_BOTTOM_LEFT_X) * MAP_2D_UI_WIDTH_PILOT / (MAP_2D_TOP_RIGHT_X - MAP_2D_BOTTOM_LEFT_X);


            g.fillOval((int) ((int) mapX + (scaledX - 1)), (int) (mapY + scaledZ - 1), 2, 2);

        }

        //Automatic moving destination
        g.setColor(Color.green);
        g.fillOval((int) destination.x - 5, (int) destination.y - 5, 10, 10);
    }

    protected boolean isPowerReversed() {
        return isPowerReversed;
    }

    protected boolean isAngleReversed() {
        return isAngleReversed;
    }

    protected void setAngleReversed(boolean setting) {
        isAngleReversed = setting;
    }

    protected boolean isHasStopped() {
        return hasStopped;
    }

    protected void setHasStoppedToFalse() {
        hasStopped = false;
    }

    protected void setIsPowerReversed(boolean setting) {
        isPowerReversed = setting;
    }

    protected boolean isRadarOn() {
        return isRadarOn;
    }

    protected void setRadarOn(boolean radarOn) {
        isRadarOn = radarOn;
    }

    private void setPilotDashboardInPilot() {
        pilot.setPilotDashboard(this);
    }

    public void setMapDisplay(MapDisplay mapDisplay) {
        this.mapDisplay = mapDisplay;
    }

    public void setAllRadarPoints(ArrayList<nl.saxion.ptbc.ObjectDisplay.Point2D> allRadarPoints) {
        this.allRadarPoints = allRadarPoints;
    }
    public ThePilotPanel getThePilotPanel() {
        return thePilotPanel;
    }

    public void setHasStoppedToTrue() {
        hasStopped = true;
    }

    /**
     * 1 = STOP
     * 2 = STOP NEXT
     * 3 = NO STOPPING
     * @return the current setting.
     */
    public int getCollisionDetectionSetting() {
        return collisionDetectionSetting;
    }

    protected void incrementCollisionSetting() {
        collisionDetectionSetting++;
    }

    public void setCollisionDetectionSettingToStop() {
        collisionDetectionSetting = 1;
    }

    /**
     * Updates the collision setting button text, to Stop, and its internal Setting.
     */
    public void updateStopButtonText() {
        functions.updateStopButtonText(ignoreCollisionBtn);
    }

    public ThePilot getPilot() {
        return pilot;
    }

    protected boolean isSwitchManual() {
        return switchManual;
    }

    protected void setSwitchManual(boolean setting) {
        this.switchManual = setting;
    }
}

