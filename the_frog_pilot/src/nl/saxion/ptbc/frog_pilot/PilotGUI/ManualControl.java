package nl.saxion.ptbc.frog_pilot.PilotGUI;

import nl.saxion.ptbc.frog_pilot.ThePilotPanel;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class ManualControl implements KeyListener {

    private final ThePilotPanel thePilotPanel;
    private boolean focusLost = false;
    private boolean up = false, left = false, down = false, right = false, stop = false;
    private int power;
    private static int angle = 0;
    private int duration = 1;
    private boolean onCoolDown = false;

    public ManualControl(ThePilotPanel thePilotPanel) {
        this.thePilotPanel = thePilotPanel;
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    /**
     * Changes boolean values to true when the corresponding button is pressed.
     * @param e the event to be processed
     */
    @Override
    public void keyPressed(KeyEvent e) {
        if (!focusLost) {
            switch (e.getKeyCode()) {
                case KeyEvent.VK_SPACE -> stop = true;
                case KeyEvent.VK_W -> up = true;
                case KeyEvent.VK_A -> left = true;
                case KeyEvent.VK_S -> down = true;
                case KeyEvent.VK_D -> right = true;
            }
            controlMovement();
        }
    }

    /**
     * Changes boolean values to false when the corresponding button is released.
     * @param e the event to be processed
     */
    @Override
    public void keyReleased(KeyEvent e) {
        //Second version
        if (!focusLost) {
            switch (e.getKeyCode()) {
                case KeyEvent.VK_SPACE -> stop = false;
                case KeyEvent.VK_W -> up = false;
                case KeyEvent.VK_A -> left = false;
                case KeyEvent.VK_S -> down = false;
                case KeyEvent.VK_D -> right = false;
            }
        }
    }

    /**
     * Sends drive commands depending on the current buttons which are pressed.
     */
    private void controlMovement() {
        //Second version
        if (stop)
            stop();
        else {

            if ((up && down) || (!up && !down))
                stop();

            if ((left && right) || (!left && !right))
                angle = 0;

            if (up)
                driveForward();
            else if (down) {
                driveBackward();
            }

            if (left)
                driveToLeft();
            else if (right) {
                driveToRight();
            }
        }
        if(!onCoolDown  && (up || down)) {
            onCoolDown = true;
            System.out.printf("PILOT DRIVE %d %d %d", power, angle, duration);
            String driveMessage = String.format("PILOT DRIVE %d %d %d", power, angle, duration);
            thePilotPanel.getPilotDashboard().getPilot().sendMessage(driveMessage, thePilotPanel.getPilotDashboard().getPilot());
        }

    }

    /**
     * Sets the parameters for drive command to stop the frog
     */
    private void stop() {
        power = 0;
        angle = 0;
    }
    /**
     * Sets the parameters for drive command to drive forward
     */
    private void driveForward() {
        power = 2000;
        angle = 0;
    }
    /**
     * Sets the parameters for drive command to drive backwards
     */
    private void driveBackward() {
        power = -2000;
        angle = 0;
    }
    /**
     * Sets the parameters for drive command to drive to the right
     */
    private void driveToRight() {
        angle = 20;
    }
    /**
     * Sets the parameters for drive command to drive to the left
     */
    private void driveToLeft() {
        angle = -20;
    }
    /**
     * Resets the frog for when the W,A,S,D movement is turned off
     */
    public void resetFrog() {
        focusLost = true;
        stop = false;
        up = false;
        left = false;
        down = false;
        right = false;
        onCoolDown = false;
        power = 0;
        angle = 0;
        duration = 1;
    }

    /**
     * Clicking outside the pilot dashboard UI will stop input.
     */
    public void focusGained() {
        focusLost = false;
    }

    /**
     * Sets the cooldown to false, so the frog accepts new input.
     */
    public void setNotOnCoolDown() {
        onCoolDown = false;
    }
}
