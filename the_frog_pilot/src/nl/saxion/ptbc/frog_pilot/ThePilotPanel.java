package nl.saxion.ptbc.frog_pilot;

import nl.saxion.ptbc.frog_pilot.PilotGUI.ManualControl;
import nl.saxion.ptbc.frog_pilot.PilotGUI.PilotDashboard;

import javax.swing.*;
import java.awt.*;

import static nl.saxion.ptbc.shared.Shared.*;

public class ThePilotPanel extends JPanel {

    private final PilotDashboard pilotDashboard;

    //KeyBoard Control
    private ManualControl manualControl;

    public ThePilotPanel(PilotDashboard pilotDashboard) {
        this.pilotDashboard = pilotDashboard;
        initClasses();
        setPanelSize();
        addKeyListener(manualControl);
    }

    private void initClasses() {
        manualControl = new ManualControl(this);
    }

    private void setPanelSize() {
        Dimension size = new Dimension(PILOT_SCREEN_WIDTH, PILOT_SCREEN_HEIGHT);
        setPreferredSize(size);
    }

    public PilotDashboard getPilotDashboard() {
        return pilotDashboard;
    }

    public ManualControl getManualControl() {
        return manualControl;
    }

    public void focusGained() {
        manualControl.focusGained();
    }

    public void focusLost() {
        manualControl.resetFrog();
    }
}
