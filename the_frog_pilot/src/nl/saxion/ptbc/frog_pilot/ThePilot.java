package nl.saxion.ptbc.frog_pilot;

import nl.saxion.ptbc.frog_pilot.AntiCollision.CollisionDetection;
import nl.saxion.ptbc.Map2D.Map;
import nl.saxion.ptbc.Map2D.MapCalculations;
import nl.saxion.ptbc.ObjectDisplay.MapDisplay;
import nl.saxion.ptbc.ObjectDisplay.Point2D;
import nl.saxion.ptbc.ObjectDisplay.Point3D;
import nl.saxion.ptbc.frog_pilot.PilotGUI.PilotDashboard;
import nl.saxion.ptbc.sockets.SasaCommunicationException;
import nl.saxion.ptbc.sockets.SasaServer;
import nl.saxion.ptbc.sockets.SasaSocketInformation;
import nl.saxion.ptbc.shared.Shared;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The base class of The Pilot.
 */
public class ThePilot implements SasaSocketInformation {
    private final SasaServer sasaServer;
    private ArrayList<String> currentRadarPoints = new ArrayList<>();
    private final ArrayList<Point2D> allRadarPoints = new ArrayList<>();
    private final ArrayList<Point2D> allRadarPointsGroundControl = new ArrayList<>();
    private ArrayList<Point2D> newRadarPointsGroundControl = new ArrayList<>();
    private Point3D radar;
    private final CollisionDetection collisionDetection;

    private PilotDashboard pilotDashboard;

    public ThePilot(int port) {
        // Setup server
        sasaServer = new SasaServer();
        sasaServer.subscribe(this);
        sasaServer.listen(port);
        System.out.println("The Pilot is listening on port " + port);
        collisionDetection = new CollisionDetection(this);
    }

    public void setPilotDashboard(PilotDashboard pilotDashboard) {
        this.pilotDashboard = pilotDashboard;
    }

    public static void main(String[] args) {
        ThePilot thePilot = new ThePilot(Shared.COMMUNICATION_PORT); // Setup server to listen to a connecting Frog

        try {
            Runtime.getRuntime().exec("TheFrog\\TheFrog.exe");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        PilotDashboard pilotDashboard = new PilotDashboard(thePilot);
        pilotDashboard.start();
    }

    /**
     * Handles the received data message. The method processes the received message and performs the necessary actions based
     * on its content. If the message starts with "[GROUND CONTROL]", it handles messages related to ground control actions.
     * Otherwise, it handles messages related to the frog and pilot actions.
     *
     * @param message the received message to handle
     */
    @Override
    public void receivedData(String message) {
        if (message.startsWith("[GROUND CONTROL]")) {
            String actionMessage = message.substring(17);
            System.out.printf("[Received From Ground Control] %s%n", actionMessage);
            if (actionMessage.contains("PILOT RADAR ON") || actionMessage.contains("PILOT RADAR OFF") || actionMessage.contains("PILOT DRIVE")) {
                sendMessage(actionMessage, this);
            }
            if (actionMessage.equals("Ground Control Requests Detected Obstacles")) {
                addOnlyNewObstaclesToArray();
                if (!newRadarPointsGroundControl.isEmpty()) {
                    for (Point2D point : newRadarPointsGroundControl) {
                        String currentMessage = "RADAR POINT " + point.getX() + " " + point.getZ();
                        sendMessageToGroundControl(currentMessage);
                    }
                    sendMessageToGroundControl("SENT ALL RADAR POINTS");
                    allRadarPointsGroundControl.addAll(newRadarPointsGroundControl);
                    newRadarPointsGroundControl = new ArrayList<>();
                }
            }
            if (actionMessage.startsWith("RADAR POINT")) {
                String[] messageSplit = actionMessage.split(" ");

                for (int i = 0; i < messageSplit.length; i++) {
                    if (!messageSplit[i].equals("RADAR") || i + 4 >= messageSplit.length) {
                        continue;
                    }

                    double x = Double.parseDouble(messageSplit[i + 2]);
                    double z = Double.parseDouble(messageSplit[i + 3]);
                    Point2D radarPoint = new Point2D(x, z);

                    allRadarPointsGroundControl.add(radarPoint);
                    allRadarPoints.add(radarPoint);
                }
                pilotDashboard.repaint();
            }
        } else {
            if (!message.startsWith("FROG POINT"))
                System.out.printf("[Received] %s%n", message);

            if (message.startsWith("FROG RADAR START")) {
                radar = takeRadarLocation(message);
            }
            if (message.startsWith("FROG POINT")) {
                currentRadarPoints.add(message);
            }
            if (message.startsWith("FROG POSITION")) {
                pilotDashboard.getThePilotPanel().getManualControl().setNotOnCoolDown();
                pilotDashboard.setHasStoppedToTrue();
                collisionDetection.setLastFrogInfo(message);

                //Map update
                updateFrogPosOnMap(message);
            }
            if (message.startsWith("FROG RADAR STOP")) {
                pilotDashboard.setMapDisplay(new MapDisplay(currentRadarPoints));
                ArrayList<Point3D> current_3D_RadarPoints = MapDisplay.convertStringTo3D(currentRadarPoints, "FROG POINT ");
                allRadarPoints.addAll(MapCalculations.convertToAbsoluteCoordinates(current_3D_RadarPoints, radar));
                Map map = new Map(allRadarPoints);
                map.displayPointsOnMap();
                pilotDashboard.setAllRadarPoints(allRadarPoints);
                pilotDashboard.repaint();
                currentRadarPoints = new ArrayList<>();
            }

            sendMessageToGroundControl(message);
        }

    }

    private void updateFrogPosOnMap(String message) {
        String[] positionParts = message.split(" ");
        double frogX = Double.parseDouble(positionParts[2].replace(",", "."));
        double frogZ = Double.parseDouble(positionParts[3].replace(",", "."));
        double angle = Double.parseDouble(positionParts[5].replace(",", "."));
        pilotDashboard.updateFrogOn2DMap(frogX, frogZ, angle);
    }

    /**
     * Sends a message to the Ground Control.
     * @param message the message to be sent
     * @throws RuntimeException if there is a SasaCommunicationException during sending
     */
    public void sendMessageToGroundControl(String message) {
        if (message.startsWith("FROG POSITION")) {
            try {
                sasaServer.send(message);
            } catch (SasaCommunicationException e) {
                throw new RuntimeException(e);
            }
        }

        if (message.startsWith("PILOT DRIVE")) {
            try {
                sasaServer.send(message);
            } catch (SasaCommunicationException e) {
                throw new RuntimeException(e);
            }
        }
        if (message.startsWith("RADAR POINT") || message.startsWith("SENT ALL RADAR POINTS")) {
            try {
                sasaServer.send(message);
            } catch (SasaCommunicationException e) {
                throw new RuntimeException(e);
            }
        }

        if (message.contains("Frog Path:") || message.contains("Frog Path First Point: ") || message.contains("Frog Path Last Point: ") || message.contains("Next Angle:") || message.contains("DODGE")) {
            try {
                sasaServer.send(message);
            } catch (SasaCommunicationException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public void statusUpdate(String client, String status) {
        System.out.printf("[State] %s: %s%n", client, status);
    }

    /**
     * Sends message to SASA, checking for collision if it's a drive command.
     * @param text the message to be sent.
     * @param pilot the pilot.
     */
    public void sendMessage(String text, ThePilot pilot) {
        int collisionSetting = pilotDashboard.getCollisionDetectionSetting();

        boolean skipCurrent = false;


        if (collisionSetting <= 2 && text.startsWith("PILOT DRIVE") && collisionDetection.checkForCollision(text)) {
            if (collisionSetting == 2) {
                pilotDashboard.setCollisionDetectionSettingToStop();
                skipCurrent = true;
            }
            if (!skipCurrent) {
                stopAutomaticMovement();
                return;
            } else {
                pilotDashboard.updateStopButtonText();
            }
        }

        System.out.println("[SEND] " + text);

        try {
            pilot.sasaServer.send(text);
        } catch (SasaCommunicationException e) {
            throw new RuntimeException(e);
        }

    }

    /**
     * Sets the automatic's movement "stopAutomatic" to true.
     */
    private void stopAutomaticMovement() {
        sendMessageToGroundControl("DODGE");
    }
    public ArrayList<Point2D> getAllRadarPoints() {
        return allRadarPoints;
    }

    // ...

    /**
     * Extracts the radar location from the given message.
     *
     * @param message the message containing the radar location
     * @return the radar location as a Point3D object
     */
    public static Point3D takeRadarLocation(String message) {
        String[] values = message.split("FROG RADAR START ")[1].split(" ");
        List<Double> convertedValues = new ArrayList<>();
        for (String value : values) {
            double convertedValue = Double.parseDouble(value.replace(",", "."));
            convertedValues.add(convertedValue);
        }
        return new Point3D(convertedValues.get(0), convertedValues.get(1), convertedValues.get(3));
    }

    /**
     * Adds only the new obstacles from the allRadarPoints list to the newRadarPointsGroundControl list.
     */
    public void addOnlyNewObstaclesToArray() {
        for (Point2D point : allRadarPoints) {
            if (!allRadarPointsGroundControl.contains(point)) {
                newRadarPointsGroundControl.add(point);
            }
        }
    }
}

