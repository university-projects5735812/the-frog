package nl.saxion.ptbc.frog_pilot.database;

/**
 * The listener interface for receiving database events.
 */
public interface DatabasePilotHandlerListener {

    /**
     * Called when a mission is saved to the database.
     *
     * @param message The message indicating the status of the mission save operation.
     */
    void onMissionSaved(String message);
}
